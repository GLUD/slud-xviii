# Codigo Fuente del app

Aquí reside todo el código fuente de la página web que estamos desarrollando, el codigo esta organizado por:

## Carpeta /app 

Aquí encontraras los componentes que se utilizan para la generación de la aplicación del la página web.

## Carpeta /assets

Auqí podras encontrar todos los elementos gráficos, videos, fuentes que se utilicen dentro de la página web.

## Carpeta /enviroments

Esta carpeta solo se utiliza o se modifica en el momento que vallamos a desplegar la página web

## Archivos de carpeta

Estos archivos son archivos globales, usualmente solo se modifica styles.csss para agregar un estilo que afecte a toda la pagina web y sus componentes

Favicon.ico -> icono de la página se puede y debe cambiar por el logo o algo que identifique la página web.

Podrás encontrar un a breve descripción de archivos por carpeta :)