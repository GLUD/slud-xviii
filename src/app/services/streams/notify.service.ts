import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';

const queries = {
  STREAM: gql`
  query {
    stream {
      isStreaming
      streamUrl
    }
  }
  `
}

@Injectable({
  providedIn: 'root'
})
export class NotifyService {

  constructor(private apollo: Apollo) {}

  public getStreamStatus() {
    return this.apollo.watchQuery({
      query: queries.STREAM,
      pollInterval: 500
    });
  }

}
