import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { PatrocinadoresComponent } from './componentes/patrocinadores/patrocinadores.component';
import { UbicacionComponent } from './componentes/ubicacion/ubicacion.component';
import { NotifyComponent } from './componentes/notify/notify.component';
import { SludComponent } from './componentes/slud/slud.component';
import { TiendaComponent } from './componentes/tienda/tienda.component';
import { RegConferencitasComponent } from './componentes/reg-conferencitas/reg-conferencitas.component';
import { EnConstruccionComponent } from './componentes/en-construccion/en-construccion.component';
import { NavBarComponent } from './componentes/global-components/nav-bar/nav-bar.component';
import { FooterComponent } from './componentes/global-components/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ParticlesModule } from 'angular-particle';
import { NgImageSliderModule } from 'ng-image-slider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StreamsNotifyComponent } from './componentes/global-components/streams-notify/streams-notify.component';
import { StreamsComponent } from './componentes/streams/streams.component';
import { GraphQLModule } from './graphql.module';
import { RecaptchaModule } from './recaptcha.module';
import { SettingsService } from './services/settings/settings.service';
import { ArticulosComponent } from './componentes/tienda/articulos/articulos.component';
import { ArticuloComponent } from './componentes/tienda/articulo/articulo.component';
import { CarruselComponent } from './componentes/carrusel/carrusel.component';
import { TimerComponent } from './componentes/timer/timer.component';
import { ModalarticuloComponent } from './componentes/tienda/modalarticulo/modalarticulo.component';
import { DynamicHostDirective } from './componentes/tienda/dynamic-host.directive';
import { DescriptionComponent } from './componentes/tienda/modalarticulo/description/description.component';

export function initApp(settingsService: SettingsService) {
  return () => settingsService.initializeApp();
}

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    PatrocinadoresComponent,
    UbicacionComponent,
    SludComponent,
    TiendaComponent,
    RegConferencitasComponent,
    EnConstruccionComponent,
    NavBarComponent,
    FooterComponent,
    StreamsNotifyComponent,
    StreamsComponent,
    NotifyComponent,
    ArticulosComponent,
    ArticuloComponent,
    CarruselComponent,
    TimerComponent,
    ModalarticuloComponent,
    DynamicHostDirective,
    DescriptionComponent,
  ],
  entryComponents: [ ModalarticuloComponent ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgImageSliderModule,
    HttpClientModule,
    ParticlesModule,
    NgbModule,
    GraphQLModule,
    RecaptchaModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initApp,
      deps: [SettingsService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
