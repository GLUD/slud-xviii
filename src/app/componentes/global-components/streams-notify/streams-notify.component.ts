import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-streams-notify',
  templateUrl: './streams-notify.component.html',
  styleUrls: ['./streams-notify.component.css']
})
export class StreamsNotifyComponent implements OnInit {

  @Input()  streamUrl: string;

  constructor() {}

  ngOnInit() {}

}
