import { IcarruselItem } from './Icarrusel-item.metadata';

export const CARRUSEL_DATA_ITEMS: IcarruselItem[] = [
  {
    id: 1,
    title: {
      first: 'TITULO',
      second: 'principal',
    },
    subtitle: 'La mejor descripcion',
    link: '/',
    image:
      'https://cdn.pixabay.com/photo/2019/01/03/19/24/landscape-3911734_960_720.jpg',
  },
  {
    id: 2,
    title: {
      first: 'TITULO2',
      second: 'principal2',
    },
    subtitle: 'La mejor descripcion2',
    link: '/',
    image:
      'https://cdn.pixabay.com/photo/2021/07/08/14/06/sea-6397001_960_720.jpg',
  },
  {
    id: 3,
    title: {
      first: 'TITULO3',
      second: 'principal3',
    },
    subtitle: 'La mejor descripcion3',
    link: '/',
    image:
      'https://cdn.pixabay.com/photo/2019/01/03/19/24/landscape-3911734_960_720.jpg',
  },
  {
    id: 3,
    title: {
      first: 'TITULO3',
      second: 'principal3',
    },
    subtitle: 'La mejor descripcion3',
    link: '/',
    image:
      'https://cdn.pixabay.com/photo/2021/07/09/04/19/girl-6398258_960_720.jpg',
  },
];
