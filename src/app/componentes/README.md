# Carpeta componentes

Aquí estarán los editables de cada una de las páginas web tenemos para desarrollar

-<> Camiseta              Breve descripción y muestra del diseño de la camiseta
-<> En-construcción       En construcción
-<> global-components     Componentes que se usan globalmente como menus y pies de pagina
-<> inicio                Página inicial y la que debe quedar mejor :D
-<> patrocinadores        Página dedicada a los patrocinadores del evento
-<> reg-conferencistas    Página para realizar el registro de conferencistas
-<> slud                  Página explicando que es el slud
-<> Ubicación             Página de donde encontrarnos y de streamming (posiblemente)

## Componentes ¿Que es eso?

Es la parte fundamental de angular se podria decir que un componente, realizando una analogia con una casa. El componente seria el ladrillo con el cual construimos la casa.

Los componentes contienen:
    Un archivo .css donde puedes modificar y agregar estilos a la pagina web.
    Un archivo .html donde se define la estructura y el llamado a otros componentes.
    Un archivo .ts donde se realiza el uso de typescrip y basicamente se programan objetos.
    Un archivo .spect.ts donde se realizan los test unitarios a nuestro componente.

### Como crear un componente 

´ng generate component <nombre del componente>´
