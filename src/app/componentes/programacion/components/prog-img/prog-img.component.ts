import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-prog-img',
  templateUrl: './prog-img.component.html',
  styleUrls: ['./prog-img.component.css']
})
export class ProgImgComponent implements OnInit {

  constructor() { }
  @Input('href')  href: string;
  ngOnInit(): void {
  }

}
