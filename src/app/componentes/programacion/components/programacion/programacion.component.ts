import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { StrapiRequestsService } from 'src/app/services/strapiGraphql/strapi-requests.service';
import { environment } from 'src/environments/environment';
import { Conferencista } from '../../agenda.model';

@Component({
  selector: 'app-programacion',
  templateUrl: './programacion.component.html',
  styleUrls: ['./programacion.component.css'],
})
export class ProgramacionComponent implements OnInit, OnDestroy {
  semana: any[];
  cientificos: any[];
  subscriptionSpeakers: Subscription;
  subscriptionScientist: Subscription;
  constructor(private dataService: StrapiRequestsService) {}

  ngOnInit() {
    this.subscriptionScientist = this.dataService
      .getCientificos()
      .valueChanges.subscribe((response: any) => {
        this.cientificos = [];

        for (let cientifico of response.data.cientificos) {
          this.cientificos.push({
            url: environment.strapiUrl + cientifico.imagenCientifico.url,
            nombre: cientifico.nombreCientifico,
          });
        }
      });

    this.subscriptionSpeakers = this.dataService
      .getConferencistas()
      .valueChanges.subscribe((response: any) => {
        const semana: { [x: string]: Conferencista[] } = {};
        const conferencistas: Conferencista[] = [];
        let dataConferencistas = response.data.conferencistaActivos;
        dataConferencistas.forEach((dataConferencista) => {
          const conferencista = new Conferencista(dataConferencista);
          let date = new Date(conferencista.FechaConferencia);
          conferencista.FechaConferencia = date.toISOString();
          conferencista.Imagen =
            environment.strapiUrl + dataConferencista?.ProfileImage?.url;
          conferencistas.push(conferencista);
        });
        conferencistas.forEach((conferencista) => {
          let date = new Date(conferencista.FechaConferencia);
          if (!semana[date.toDateString()]) {
            semana[date.toDateString()] = [];
          }
          semana[date.toDateString()].push(conferencista);
        });
        this.semana = ordenar(semana);
      });
  }

  ngOnDestroy(): void {
    this.subscriptionSpeakers.unsubscribe();
    this.subscriptionScientist.unsubscribe();
  }
}

function ordenar(data: {[x: string]: Conferencista[]}): { day: string; conferencistas: Conferencista[] }[] {
  let array: { day: string; conferencistas: Conferencista[] }[] = [];
  for (let day in data) {
    //ordena las horas de ascendente
    data[day].map((conferencista) => {
      let date = new Date(conferencista.FechaConferencia);
      conferencista.FechaConferencia = date.getHours()*100 + (date.getMinutes());
      data[day].sort((a,b)=> a.FechaConferencia - b.FechaConferencia);
    });
    // agrega el formato de : a las horas 
    data[day].map((conferencista)=>{ 
      let tempConferencista: String = conferencista.FechaConferencia.toString();

      if(conferencista.FechaConferencia >= 1000)
        conferencista.FechaConferencia = tempConferencista.slice(0,2) + ':' + tempConferencista.slice(2); 
      else
        conferencista.FechaConferencia = tempConferencista.slice(0,1) + ':' + tempConferencista.slice(1); 
      
    });
    array.push({day: day, conferencistas: data[day] }); 
  }

  array = array.sort(
    (a, b) => new Date(a.day).getTime() - new Date(b.day).getTime()
  );

  array.map((days) => {
    days.day = toSpanishDate(new Date(days.day));
  });

  return array;
}


function toSpanishDate(date: Date): string {
  const month = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
  ];
  const day = [
    'Domingo',
    'Lunes',
    'Martes',
    'Miercoles',
    'Jueves',
    'Viernes',
    'Sabado',
  ];
  return (
    day[date.getDay()] + ' ' + date.getDate() + ' de ' + month[date.getMonth()]
  );
}
