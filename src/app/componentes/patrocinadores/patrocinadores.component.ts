import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { StrapiRequestsService } from 'src/app/services/strapiGraphql/strapi-requests.service';
import { environment } from 'src/environments/environment';

class Sponsor {
  name: string;
  description: string;
  imagen: string;
  alternativeText: string;

  constructor(object) {
    this.name = object.Nombre;
    this.description = object.Description;
    this.imagen = environment.strapiUrl + object.Imagen.url;
    this.alternativeText = object.id;
  }


}

@Component({

  selector: 'app-patrocinadores',
  templateUrl: './patrocinadores.component.html',
  styleUrls: ['./patrocinadores.component.css']
})

export class PatrocinadoresComponent implements OnInit, OnDestroy {

  stateNotify: boolean = false;
  element: HTMLElement;
  subscription: Subscription;

  sponsors: Sponsor[];

  constructor(private sponsorsService: StrapiRequestsService) {}

  getAlert(nombre) {
    this.element = document.getElementById(nombre) as HTMLElement;

    this.element.style.display = !this.stateNotify? 'block' : 'none';

    this.stateNotify = !this.stateNotify;
  }

  ngOnInit(): void {
    this.subscription = this.sponsorsService.getPatrocinadores().valueChanges.subscribe((response: any) => {
      const sponsorsData = response.data.patrocinadors;
      
      this.sponsors = [];
      sponsorsData.forEach(sponsor => {
        this.sponsors.push(new Sponsor(sponsor));
      });

    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }



}
