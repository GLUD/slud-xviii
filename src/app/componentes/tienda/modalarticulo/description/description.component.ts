import { Component, OnInit , Input} from '@angular/core';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
export class DescriptionComponent implements OnInit {

  @Input() title = '';
  @Input() description = '';
  @Input() stock = 0;
  @Input() price = 0;

  constructor() { }

  ngOnInit(): void {
  }

  toCop(value) {
    const formatter = new Intl.NumberFormat('es-CO', {
        style: 'currency',
        currency: 'COP'
    })

    return formatter.format(value)
  }

}
