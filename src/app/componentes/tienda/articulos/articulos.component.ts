import { Component, OnInit, ViewChild, ComponentFactoryResolver, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ModalarticuloComponent } from '../modalarticulo/modalarticulo.component';
import { ARTICULOS_DATA_ITEMS } from './articulos.const';
import { IarticuloItem } from './Iarticulo-item.metadata';
import { DynamicHostDirective } from '../dynamic-host.directive';
import { StrapiRequestsService } from 'src/app/services/strapiGraphql/strapi-requests.service';
import { environment } from 'src/environments/environment';
import { IcarruselItem } from '../../carrusel/Icarrusel-item.metadata';
@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.component.html',
  styleUrls: ['./articulos.component.css'],
})
export class ArticulosComponent implements OnInit, OnDestroy {

  @ViewChild(DynamicHostDirective) public dinamyHost: DynamicHostDirective;

  private subscription: Subscription;

  public articulosData: IarticuloItem[];

  constructor(private componentFactoryResolver: ComponentFactoryResolver, private strapiService: StrapiRequestsService) { }

  ngOnInit(): void {
    this.subscription = this.strapiService.getStoreArticles().valueChanges.subscribe(response => {
      this.articulosData = [];

      let articles = (response.data as any).storeArticles;
      for (let article of articles) {
        let images = [];
        for (let image of article.images) {
          images.push({ id: image.id, image: environment.strapiUrl + image.url });
        }
        this.articulosData.push(new IarticuloItem(article, images));
      }

      this.articulosData.map((article) => {
        article.image = environment.strapiUrl + article.image.url;
        article.images.map(image => {
          image = new IcarruselItem({ id: image.id, url: environment.strapiUrl + image.image });
        });
      });
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  async openModal(e) {
    const modalArticle = this.componentFactoryResolver.resolveComponentFactory(ModalarticuloComponent);
    this.dinamyHost.viewContainerRef.clear();
    const componente = this.dinamyHost.viewContainerRef.createComponent(modalArticle);
    const articulo = this.articulosData.find(i => i.id == e);
    componente.instance.setDescription(articulo);
    componente.instance.setcarruselData(articulo.images);

  }


}
