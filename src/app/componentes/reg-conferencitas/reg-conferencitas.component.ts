import { Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { StrapiRequestsService } from '../../services/strapiGraphql/strapi-requests.service';

import { ReCaptchaV3Service } from 'ng-recaptcha';
import { HttpClient } from '@angular/common/http';
import { SingletonSettings } from 'src/app/services/settings/settings.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-reg-conferencitas',
  templateUrl: './reg-conferencitas.component.html',
  styleUrls: ['./reg-conferencitas.component.css'],
})
export class RegConferencitasComponent implements OnDestroy {
  conferencistaForm: FormGroup;
  tipoDocumento: { [x: string]: string }[];
  horaDisponible: { [x: string]: string | number }[];
  strapiSubscription: Subscription;
  recaptchaSubscription: Subscription;

  constructor(
    private router: Router,
    private conferencistasService: StrapiRequestsService,
    private reCaptchaV3Service: ReCaptchaV3Service,
    private httpClient: HttpClient
  ) {
    try {
      this.conferencistaForm = new FormGroup({
        nombreCompleto: new FormControl('', [Validators.required]),
        tipoIdentificacion: new FormControl('', [Validators.required]),
        numeroIdentificacion: new FormControl('', [Validators.required, Validators.minLength(8)]),
        email: new FormControl('', [Validators.required, Validators.email]),
        perfilProfesional: new FormControl('', [Validators.required]),
        linkRedSocial: new FormControl(),
        celular: new FormControl('', [Validators.required, Validators.minLength(10)]),
        nombreConferencia: new FormControl('', [Validators.required]),
        descripcionConferencia: new FormControl('', [Validators.required]),
        dia: new FormControl('', [Validators.required]),
        hora: new FormControl('', [Validators.required]),
        requerimientos: new FormControl(),
        patrocinador: new FormControl('', [Validators.required]),
        logoOrganizacion: new FormControl(),
        imagenPerfil: new FormControl(),
        comentarios: new FormControl(),
      });
    } catch (error) { console.log('Error de validacion.') }

    this.tipoDocumento = [
      { value: 'Identidad', name: 'Tarjeta de Identificación' },
      { value: 'Cedula', name: 'Cédula de Ciudadanía' },
      { value: 'Extranjera', name: 'Cédula Extranjera' },
    ];

    this.horaDisponible = [
      { value: 10, name: '10 - 11 a.m.' },
      { value: 11, name: '11 - 12 m.' },
      { value: 12, name: '12 - 1 p.m.' },
      { value: 13, name: '1 - 2 p.m.' },
      { value: 14, name: '2 - 3 p.m.' },
      { value: 15, name: '3 - 4 p.m.' },
      { value: 16, name: '4 - 5 p.m.' },
      { value: 17, name: '5 - 6 p.m.' },
    ];
  }

  ngOnDestroy(): void {
    if (this.strapiSubscription) {
      this.strapiSubscription.unsubscribe();
    }
    if (this.recaptchaSubscription) {
      this.recaptchaSubscription.unsubscribe();
    }
  }

  onChangeFile(event) {
    const files = event.target.files;
    if (files.length > 0) {
      const file = files[0];
      this.conferencistaForm.patchValue({
        logoOrganizacion: file,
      });
    }
  }

  onChangeFileProfile(event) {
    const files = event.target.files;
    if (files.length > 0) {
      const file = files[0];
      this.conferencistaForm.patchValue({
        imagenPerfil: file,
      });
    }
  }

  validarDatos() {
    let datos = this.conferencistaForm.value;
    if (
      datos.nombreCompleto == '' ||
      datos.tipoIdentificacion == '' ||
      datos.numeroIdentificacion == '' ||
      datos.email == '' ||
      datos.perfilProfesional == '' ||
      datos.celular == ''
    ) {
      alert('Error en los datos: Informacion personal.');
      return false;
    }

    if (
      datos.nombreConferencia == '' ||
      datos.descripcionConferencia == '' ||
      datos.dia == '' ||
      datos.hora == ''
    ) {
      alert('Error en los datos: Informacion de la conferencia.');
      return false;
    }

    if (
      datos.patrocinador === '' ||
      datos.requerimientos == null ||
      datos.requerimientos == ''
    ) {
      alert('Error en los datos: Requerimientos para dar la conferencia.');
      return false;
    }

    return true;
  }

  async enviarDatos() {
    if (this.validarDatos()) {
      try {
        if (this.conferencistaForm.valid) {
          this.recaptchaSubscription = this.reCaptchaV3Service.execute('registerSpeaker')
            .subscribe((token) => {
              this.httpClient.get(environment.recaptchaUrl, {
                params: {
                  secret: SingletonSettings.singletonSettings.settings.secretKey,
                  response: token
                },
              }).subscribe(async ({ success, score }: { success: boolean, score: number }) => {
                if (success && score >= 0.9) {
                  this.strapiSubscription = (
                    await this.conferencistasService.addConferencista(
                      this.conferencistaForm.value
                    )
                  ).subscribe(({ data }) => {
                    if (data) {
                      alert('Datos subidos satisfactoriamente');
                      this.router.navigateByUrl('/');
                    }
                  });
                } else {
                  alert("¿Eres un robot?");
                }
              });
            });
        }
      } catch (error) {
        alert(
          'Error al enviar sus datos, intente nuevamente mas tarde.' + error
        );
      }
    }
  }
}