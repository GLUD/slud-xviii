import { Component, OnInit, ViewChild } from '@angular/core';
import { NgImageSliderComponent } from 'ng-image-slider';

@Component({
  selector: 'app-slud',
  templateUrl: './slud.component.html',
  styleUrls: ['./slud.component.css']
  
})
export class SludComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  @ViewChild('nav') slider: NgImageSliderComponent;
  

  imageObject: Array<object> = [
    {
    image: '/assets/img/afiches/SLUDII.jpg',
    thumbImage: 'assets/img/afiches/SLUDII.jpg',
    alt: 'alt of image',
    title: 'SLUDII'
    }, 
    {
      image: '/assets/img/afiches/SLUDIII.jpg',
      thumbImage: 'assets/img/afiches/SLUDIII.jpg',
      alt: 'alt of image',
      title: 'SLUDIII'
    },
    {
      image: '/assets/img/afiches/SLUDIV.jpg',
      thumbImage: 'assets/img/afiches/SLUDIV.jpg',
      alt: 'alt of image',
      title: 'SLUDIV'
    },
    {
      image: '/assets/img/afiches/SLUDV.jpg',
      thumbImage: 'assets/img/afiches/SLUDV.jpg',
      alt: 'alt of image',
      title: 'SLUDV'
    },
    {
      image: '/assets/img/afiches/SLUDVI.jpg',
      thumbImage: 'assets/img/afiches/SLUDVI.jpg',
      alt: 'alt of image',
      title: 'SLUDVI'
    },
    {
      image: '/assets/img/afiches/SLUDVII.png',
      thumbImage: 'assets/img/afiches/SLUDVII.png',
      alt: 'alt of image',
      title: 'SLUDVII'
    },
    {
      image: '/assets/img/afiches/SLUDIX.jpg',
      thumbImage: 'assets/img/afiches/SLUDIX.jpg',
      alt: 'alt of image',
      title: 'SLUDIX'
    },
    {
      image: '/assets/img/afiches/SLUDX.jpg',
      thumbImage: 'assets/img/afiches/SLUDX.jpg',
      alt: 'alt of image',
      title: 'SLUDX'
    },
    {
      image: '/assets/img/afiches/SLUDXI.jpg',
      thumbImage: 'assets/img/afiches/SLUDXI.jpg',
      alt: 'alt of image',
      title: 'SLUDXI'
    },
    {
      image: '/assets/img/afiches/SLUDXII.png',
      thumbImage: 'assets/img/afiches/SLUDXII.png',
      alt: 'alt of image',
      title: 'SLUDXII'
    },
    {
      image: '/assets/img/afiches/SLUDXIII.jpg',
      thumbImage: 'assets/img/afiches/SLUDXIII.jpg',
      alt: 'alt of image',
      title: 'SLUDXIII'
    },
    {
      image: '/assets/img/afiches/SLUDXIV.png',
      thumbImage: 'assets/img/afiches/SLUDXIV.png',
      alt: 'alt of image',
      title: 'SLUDXIV'
    },
    {
      image: '/assets/img/afiches/SLUDXV.png',
      thumbImage: 'assets/img/afiches/SLUDXV.png',
      alt: 'alt of image',
      title: 'SLUDXV'
    },
    {
      image: '/assets/img/afiches/SLUDXVI.png',
      thumbImage: 'assets/img/afiches/SLUDXVI.png',
      alt: 'alt of image',
      title: 'SLUDXVI'
    },
    {
      image: '/assets/img/afiches/SLUDXVII.jpg',
      thumbImage: 'assets/img/afiches/SLUDXVII.jpg',
      alt: 'alt of image',
      title: 'SLUDXVII'
    }
    
  ];

  prevImageClick() {
    this.slider.prev();
  }

  nextImageClick() {
    this.slider.next();
  }

}
